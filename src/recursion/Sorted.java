package recursion;

/**
 * Program to check given array is sorted or not
 */
public class Sorted {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 10, 9, 12};
        System.out.println(sorted(arr, 0));
    }

    private static boolean sorted(int[] arr, int index) {
        if(index == arr.length-1)
            return true;
        return arr[index] < arr[index + 1] && sorted(arr, index + 1);
    }
}
