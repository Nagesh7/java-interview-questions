package recursion;

public class Permutation {

    public static void main(String[] args) {
        String str = "ABC";
        if(str == null || str.length() == 0)
            System.out.println("Not a Valid String");
        permutation(str.toCharArray(), 0);
    }

    private static void permutation(char[] chars, int currentIndex) {

        if(currentIndex == chars.length - 1) {
            System.out.println(String.valueOf(chars));
            return;
        }

        for(int i = currentIndex; i<chars.length; i++) {
            swap(chars, currentIndex, i);
            permutation(chars, currentIndex+1);
            swap(chars, currentIndex, i);
        }
    }

    private static void swap(char[] chars, int currentIndex, int j) {
        char temp = chars[currentIndex];
        chars[currentIndex] = chars[j];
        chars[j] = temp;
    }
}
