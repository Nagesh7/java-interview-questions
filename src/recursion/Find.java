package recursion;

import java.sql.SQLOutput;
import java.util.ArrayList;

public class Find {

    public static void main(String[] args) {
        int[] arr = {12, 17, 18, 9, 2, 5};
        int[] dupArr = {12, 17, 18, 9, 2, 5, 18};
        int target = 18;
        System.out.println(find(arr, 0, target));
        System.out.println(findIndex(arr, 0, target));
        System.out.println(findAllIndex(dupArr, 0, target, new ArrayList<>()));
    }

    private static boolean find(int[] arr, int index, int target) {
        if(index == arr.length)
            return false;
        return arr[index] == target || find(arr, index +1, target);
    }

    private static int findIndex(int[] arr, int index, int target) {
        if(index == arr.length)
            return -1;
        if(arr[index] == target )
            return index;
        return findIndex(arr, index +1, target);
    }

    private static ArrayList findAllIndex(int[] arr, int index, int target, ArrayList<Integer> list) {
        if(index == arr.length)
            return list;
        if(arr[index] == target )
            list.add(index);
        return findAllIndex(arr, index +1, target, list);
    }

}
