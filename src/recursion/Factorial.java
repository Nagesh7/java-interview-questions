package recursion;

public class Factorial {

    public static void main(String[] args) {
        long factorial = factorial(5);
        System.out.println("Factorial:"+factorial);
    }

    private static long factorial(int n) {
        if(n == 0 || n == 1)
            return 1;
        return factorial(n-1)*n;
    }
}
