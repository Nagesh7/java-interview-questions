package recursion;

import java.util.ArrayList;

public class FindPairs {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 4};
        int target = 5;
        System.out.println(findPairs(arr, 0, 1, target, new ArrayList<String>()));
    }

    private static ArrayList findPairs(int[] arr, int start, int end, int target, ArrayList<String> pairs) {
        if(start == arr.length || end == arr.length)
            return pairs;
        if(arr[start] + arr[end] == target) {
            pairs.add("("+start+", "+end+")");
            return findPairs(arr, start+1, start+2, target, pairs);
        }
        if(end == arr.length)
            return findPairs(arr, start+1, start+2, target, pairs);
        return findPairs(arr, start, end+1, target, pairs);
    }
}
