package dp;

public class LongestPalindromeSubString {

    public static void main(String[] args) {
        String str = "babad";
        String longestPalindrome = findLongestPalindrome(str);
        System.out.println(longestPalindrome);
    }

    private static String findLongestPalindrome(String str) {
        int n = str.length();
        String longestPalindrome = str.substring(0, 1);
        for(int i = 0; i<n; i++) {
            String palindrome = findPalindrome(str, i, i);
            if(longestPalindrome.length() < palindrome.length())
                longestPalindrome = palindrome;
            palindrome = findPalindrome(str, i, i+1);
            if(longestPalindrome.length() < palindrome.length())
                longestPalindrome = palindrome;
        }
        return longestPalindrome;
    }

    private static String findPalindrome(String str, int left, int right) {
        if(left > right)
            return null;
        while(left >= 0 && right < str.length() && str.charAt(left) == str.charAt(right)) {
            left--;
            right++;
        }
        return str.substring(left+1, right);
    }
}
