package dp;

public class LongestBalancedParentheses {

    public static void main(String[] args) {
        String str = "";
        int length = findLongestBalancedParentheses(str);
        System.out.println(length);
    }

    private static int findLongestBalancedParentheses(String str) {
        int n = str.length();
        int maxLength = 0;
        for(int i =0; i<n; i++) {
            String balancedParentheses = findBalancedParentheses(str, i, i+1);
            if(balancedParentheses.length() > maxLength)
                maxLength = balancedParentheses.length();
        }

        return maxLength;
    }

    private static String findBalancedParentheses(String str, int left, int right) {
        if(left > right)
            return null;
        while(left >= 0 && right < str.length() && ((str.charAt(left) == '(' && str.charAt(right) == ')') || (str.charAt(left) == ')' && str.charAt(right) == '('))) {
            left--;
            right++;
        }
        return str.substring(left+1, right);
    }
}
