package leetcode;

public class PalindromeNumber {

    public static void main(String[] args) {
        System.out.println("Result 1 ===>"+isPalindrome(-121));
        System.out.println("Result 2 ===>"+isPalindrome(121));
        System.out.println("Result 3 ===>"+isPalindrome(1234321));

    }

    public static boolean isPalindrome(int x) {
        if(x < 0)
            return false;
        int result = 0;
        int num = x;
        while(x != 0) {
            int reminder = x%10;
            result = result*10+reminder;
            x = x/10;
        }
        return num == result;
    }
}
