package leetcode;

public class NumIsland {

    public static void main(String[] args) {
        char[][] grid = {{'1', '1', '0', '0', '0'}, {'1', '1', '0', '0', '0'}, {'0', '0', '1', '0', '0'}, {'0', '0', '0', '1', '1'}};
        int count = numIslands(grid);
        System.out.println(count);
    }

    public static int numIslands(char[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        int result =0;
        for(int i =0; i<row; i++) {
            for(int j = 0;j<col; j++) {
                if(grid[i][j] == '1') {
                    dfs(grid, i, j, row);
                    result ++;
                }
            }
        }
        return result;
    }

    public static void dfs(char[][] grid, int i, int j, int row) {
        if(i < 0 || i>=row ||  j<0 || j>=grid[i].length || grid[i][j] != '1')
            return;
        grid[i][j] = '0';
        dfs(grid, i+1, j, row);
        dfs(grid, i-1, j, row);
        dfs(grid, i, j+1, row);
        dfs(grid, i, j-1, row);
    }
}
