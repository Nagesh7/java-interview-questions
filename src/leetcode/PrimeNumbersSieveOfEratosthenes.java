package leetcode;

public class PrimeNumbersSieveOfEratosthenes {

    public static void main(String[] args) {
        int num = 50;
        findAllPrimeNumbers(num);
    }

    private static void findAllPrimeNumbers(int num) {
        boolean[] bool = new boolean[num + 1];
        for(int i =0; i<=num; i++)
            bool[i] = true;
        for(int i =2; i <=num; i++) {
            if(bool[i] == true){
                for(int j = i*i; j<=num; j+=i) {
                    bool[j] = false;
                }

            }
        }
        for(int i = 2; i<num; i++) {
            if (bool[i] == true)
                System.out.println(i);
        }

    }


}
