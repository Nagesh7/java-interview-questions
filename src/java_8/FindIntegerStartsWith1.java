package java_8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FindIntegerStartsWith1 {

    public static List<String> findIntegerStartsWith1(List<Integer> integerList) {
        return integerList.stream()
                .map(num -> num+"")
                .filter(num->num.startsWith("1"))
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(10, 20, 30, 15, 115, 75, 1005);
        System.out.println("Numbers start with 1 List===>"+findIntegerStartsWith1(integerList));
    }
}
