package java_8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FindEvenNumbers {

    public static List<Integer> findEvenNumbers(List<Integer> integerList) {
        return integerList.stream()
                .filter(num -> num%2 == 0)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(10, 20, 30, 15, 115, 75, 1005);
        System.out.println("Even List===>"+findEvenNumbers(integerList));
    }
}
