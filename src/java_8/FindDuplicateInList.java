package java_8;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FindDuplicateInList {

    public static List<Integer> findDuplicateInList(List<Integer> integerList) {
        return integerList.stream()
                .collect(Collectors.groupingBy(
                        Function.identity(), Collectors.counting()
                ))
                .entrySet()
                .stream()
                .filter(val ->val.getValue() >1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(1, 2, 2, 3, 3, 4, 5, 6, 7, 7);
        System.out.println("Duplicate Element List:"+findDuplicateInList(integerList));
    }
}
