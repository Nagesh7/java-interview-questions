package java_8;

import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FindCharFrequencyInString {

    public static void main(String[] args) {

        String str = "Nikhil";
        Character findChar = 'i';

        str.chars().mapToObj(ch ->(char)ch)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream().filter(entrySet -> entrySet.getKey().equals(findChar))
                .forEach(System.out::println);
    }
}
