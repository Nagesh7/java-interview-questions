package arrays;

import java.util.Arrays;

public class RotateArray {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        arrayRotation(array, 3);
        Arrays.stream(array).forEach(System.out::println);
    }

    public static void arrayRotation(int[] array, int numOfRotations) {
        if (numOfRotations <= 0)
            return;
        rotateArray(array);
        arrayRotation(array, numOfRotations - 1);
    }

    private static void rotateArray(int[] array) {
        int size = array.length;
        int lastVal = array[size - 1];
        for (int i = size - 1; i >= 0; i--) {
            if (i == 0)
                array[i] = lastVal;
            else
                array[i] = array[i - 1];
        }
    }
}
