package string;

import java.util.HashSet;

public class AllSubStringsOfString {

    public static void main(String[] args) {
        String str = "Hello";
        HashSet<String> strSet = new HashSet<>();
        for(int i =0; i<str.length(); i++) {
            int maxCharLimit = i;
            StringBuffer strBuffer = new StringBuffer();
            while(maxCharLimit < 100 && maxCharLimit < str.length()) {
                strBuffer.append(str.charAt(maxCharLimit));
                if(strSet.add(strBuffer.toString())) {
                    System.out.println(strBuffer);
                }
                maxCharLimit++;
            }

        }
    }
}
