package string;

public class FindLargestNonRepeatedString {

    public static void main(String[] args) {
        String str = "abcdeabbdefghi";
        int prevLength = 0;
        String finalString = "";
        StringBuilder stringBuilder = new StringBuilder("");
        for(int i = 0; i < str.length(); i++) {
            if((stringBuilder.toString().indexOf(str.charAt(i)) != -1) || i == str.length()-1) {
                if(prevLength < stringBuilder.toString().length()) {
                    finalString = stringBuilder.toString();
                    prevLength = stringBuilder.toString().length();
                }
                stringBuilder.delete(0, stringBuilder.toString().length());
            }
            stringBuilder.append(str.charAt(i));
        }
        System.out.println(finalString);
    }
}
